Gem::Specification.new do |s|
  s.name        = 'backdown'
  s.version     = '0.0.13'
  s.date        = '2018-03-17'
  s.summary     = "Allows backups to be stored offsite and restored"
  s.description = "Can send DB backups to e.g. S3 and restore them again"
  s.authors     = ["Roman Almeida"]
  s.email       = 'post@romanalmeida.com'
  s.files       = ["lib/backdown.rb"]
  s.homepage    =
    'http://rubygems.org/gems/backdown'
  s.license       = 'MIT'

  s.add_runtime_dependency "fog-aws", ">= 1.0"
end
