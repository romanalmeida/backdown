require 'fog-aws'

class Backdown
  def initialize(bucket:, backup_folder: nil, bucket_region: 'eu-central-1', verbose: true, importer: 'legacy')
    @storage = Fog::Storage.new({
        provider:              'AWS',
        aws_access_key_id:     ENV["AWS_ACCESS_KEY_ID"],
        aws_secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"],
        region:                bucket_region
      }).directories.get(bucket)
    # E.g. "backup" or "customername_backup" depending on how the buckets are used
    @remote_backup_folder = backup_folder
    @verbose = verbose
    @local_backup = FileUtils.mkdir_p("/app/tmp/backup")
    @local_tmp = FileUtils.mkdir_p("/app/tmp/")
    @utilities = {
      pg_dump: "pg_dump",
      psql:    "psql"
    }
    @importer = importer
  end

  def backup_db(environment: ENV.fetch('RAILS_ENV') { raise "ERROR: environment not set: e.g. `development`"})
    backup_name = db_name(environment, Date.today)
    filename = File.join(@local_backup, "#{backup_name}.dump.out")
    system dump_db_command(filename)
    system "gzip -f #{filename}"
    file = @storage.files.create(
        :key => remote_db_name(backup_name),
        :body => File.open(filename + ".gz"),
        :public => false
        )
    file.save
  end

  def restore_db(backup_name: nil, days_old: 0, restore_from: 'production')
    backup_name ||= find_latest_backup(environment: restore_from, days_old: days_old)
    vputs "Downloading DB - #{backup_name}"
    s3_file = @storage.files.get(remote_db_name(backup_name))
    local_path = File.join(@local_tmp, "restore_#{backup_name}.dump.out.gz")
    File.open(local_path, 'wb') do |local_file|
      local_file.write(s3_file.body)
    end

    # Load the db into dev
    vputs "Restoring DB - #{backup_name}"
    system "gunzip -qvf #{local_path}"
    local_path = local_path.gsub('.gz', '')
    vputs restore_db_command(local_path)
    system restore_db_command(local_path)
    File.delete(local_path)
  end

  private

  def find_latest_backup(environment:, days_old:)
    date = Date.today
    date = date - days_old.to_i.days # Allows us to also get older version
    until @storage.files.head(remote_db_name(db_name(environment, date)))
      date = date - 1.day
      vputs "Trying DB for #{date}"
    end
    return db_name(environment, date)
  end

  def vputs(string)
    puts string if @verbose
  end

  def dump_db_command(filename)
    cmd = []
    cmd << "PGPASSWORD=#{ENV["DATA_DB_PASS"]}"
    cmd << @utilities[:pg_dump]
    cmd << "--dbname "   + ENV["DATA_DB_NAME"]
    cmd << "--username " + ENV["DATA_DB_USER"]
    cmd << "--host "     + ENV['DATA_DB_HOST']
    cmd << "> " + filename
    cmd.join(" ")
  end

  def restore_db_command(sql_data_file)
    case @importer
    when 'legacy' then legacy_restore_db_command(sql_data_file)
    when 'dokku' then dokku_restore_db_command(sql_data_file)
    else raise "Unknown importer: #{@importer}"
    end
  end

  def dokku_restore_db_command(sql_data_file)
    "psql #{ENV["DATABASE_URL"]} < #{sql_data_file}"
  end

  def legacy_restore_db_command(sql_data_file)
    cmd = []
    cmd << "PGPASSWORD=#{ENV["DATA_DB_PASS"]}"
    cmd << @utilities[:psql]
    cmd << "--dbname "   + ENV["DATA_DB_NAME"]
    cmd << "--username " + ENV["DATA_DB_USER"]
    cmd << "--host "     + ENV['DATA_DB_HOST']
    cmd << "--file #{sql_data_file}"
    cmd.join(" ")
  end

  def db_name(environment, date)
    [environment, date].join("_")
  end

  def remote_db_name(db_name)
    [@remote_backup_folder, db_name].compact.join("/")
  end

end
